package exerelin;

/**
 * Stores shared constants
 * @author Histidine
 */
public class ExerelinConstants {
	public static final String PLAYER_NPC_ID = "player_npc";
	public static final String AVESTA_ID = "exipirated_avesta";
	@Deprecated public static final String TAG_UNINVADABLE = "nex_uninvadable";
	public static final String MEMORY_KEY_UNINVADABLE = "$nex_uninvadable";
	public static final String MEMORY_KEY_VISITED_BEFORE = "$visitedBefore";
}
