# README #

Nexerelin is a mod for the game [Starsector](http://fractalsoftworks.com). It implements 4X-style gameplay with faction wars, diplomacy and planetary conquest.

Current release version: v0.8.1b

### Setup instructions ###
Check out the repo to Starsector/mods/Exerelin (or some other folder name) and it can be played immediately. 

Create a project with Exerelin/jars/sources/ExerelinCore as a source folder to compile the Java files.

### License ###
The Prism Freeport code and art assets are taken or adapted from the [Scy Nation mod](http://fractalsoftworks.com/forum/index.php?topic=8010.0) by Tartiflette and licensed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license](https://creativecommons.org/licenses/by-nc-sa/4.0/).

All other code is licensed under the [MIT License (Expat License version)](https://opensource.org/licenses/MIT) except where otherwise specified.

All other assets are released as [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/) unless otherwise specified.
